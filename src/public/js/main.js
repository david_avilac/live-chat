$(function (){
    const socket = io();

    // obteniendo los elementos de DOM desde la interfaz 
    const $messageForm = $('#message-form');
    const $messageBox = $('#message');
    const $chat = $('#chat');

    // obteniendo los elementos de DOM desde la LOGIN
    const $nickForm =  $('#nickForm');
    const $nickError =  $('#nickError');
    const $nickname =  $('#nickname');

    const $users = $('#usernames')

    $nickForm.submit (e => {
        e.preventDefault();
        socket.emit('new user', $nickname.val(), data => {
            if (data){
                $('#nickWrap').hide();
                $('#contentWrap').show();
            }else{
                $nickError.html(`
                <div class = "alert alert-danger">Usuario ya existente
                </div>
                `);
            }
            $nickname.val('');
        });

    });

    // Eventos
    $messageForm.submit(e => {
        e.preventDefault();
        socket.emit('send message', $messageBox.val(), data => {
            $chat.append(`<p class = "error">${data}</p>`);
        });
        $messageBox.val('');
    });

    socket.on('new message', (data) =>{
        displayMsg(data);
    });

    socket.on('usernames', data => {
        let html ='';
        for (let i = 0; i<data.length; i++){
            html += `<p><i class="fa-solid fa-ghost"></i> ${data[i]}</p>`
        }
        $users.html(html);
    });


    socket.on('whisper', data => {
        $chat.append(`<p class ="whisper"><b>${data.nick}: </b> ${data.msg}</p>`);
    })

    socket.on('load old msgs', msgs => {
        for (let i = msgs.length - 1; i >= 0; i--) {
            displayMsg(msgs[i])
        }
    })

    function displayMsg(data) {
        $chat.append(
          `<p><b>${data.nick}</b>: ${data.msg}</p>`
        );
        
      }
})