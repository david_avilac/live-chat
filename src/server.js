const http = require('http');
const path = require('path');

const express = require('express'); 
const socketio = require('socket.io');

const mongoose = require('mongoose');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

// Conexion a la DB
mongoose.connect('mongodb+srv://Alvarito:1234@clusterprueba.yavhuqa.mongodb.net/?retryWrites=true&w=majority')
    .then(db => console.log('db is connected'))
    .catch(err => console.error(err));

// Configuración
app.set('port', process.env.PORT || 8080);

require('./sockets')(io);


// Enviado archivos estaticos
app.use(express.static(path.join(__dirname, 'public')));

// Empezando el servidor  
server.listen(app.get('port'), () => { 
    console.log('Servidor en puerto', app.get('port'));
});